#!/bin/bash

TZDATA_REGION=America/Los_Angeles

# set timezone in case tex files have timestamps
ln -fs /usr/share/zoneinfo/$TZDATA_REGION /etc/localtime

apt-get update
