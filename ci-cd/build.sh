#!/bin/bash

# how many cores to use when building
PROCS=$(($(nproc) + 1))

# make agda happy if it didn't install this for some reason
touch /usr/share/libghc-agda-dev/agda.sty

make # -j $PROCS
