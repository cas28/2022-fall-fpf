\input{include/header.tex}

% auto-generated glossary terms
\input{notes/10.glt}

% rules of inference
\input{include/theories/iplc.tex}


\indextitle{Lambda calculus and type theory}
\subtitle{Our first real programs}

\makeglossaries


\begin{document}

\longwarning{This is a work-in-progress draft! You're welcome to read ahead, just be warned that these notes will get updated before we cover them in lecture.}

\maketitle
\tableofcontents

\newpage



\section{Introduction}


  We've been teasing the idea of \vocab{computation} for a while, and it's time to finally dive in. What exactly is the connection between \vocab{computation} and \vocab{formal-logic}?

  To be precise, we should remember that there are many different meaningful \vocabs{proof-system}: we've already seen \acrshort{ipl} and \acrshort{cpl} as two different examples.

  You might also know that there are many different meaningful systems of computation. Most of our modern computing hardware is based on the theory of \vocabs{Turing-machine}, but some machines like FPGAs and quantum computers are based on different foundations.

  In general, \insist{different systems of logic relate to different systems of computation}. \acrshort{ipl} and \acrshort{cpl} each give different insights into ``what computation is''.

  In this course, the main system of computation that we'll focus on is called \vocab{lambda-calculus}. Lambda calculus is a \vocab{programming-language} in a very meaningful sense, but it actually predates the entire concept of ``programming'': it was originally studied in the setting of \vocab{formal-logic}, and it took several decades for people to discover its full computational extent.

  In these notes, we'll introduce the language of \vocabstyle{\acrfull{stlc}} and show how it corresponds to \acrshort{ipl}. We'll also get to use Agda in a much more direct way than before, and we'll begin to explore its use as a \vocab{programming-language} while we continue to explore it as a \vocab{proof-language}.



\section{Proof terms}


  \Vocabs{proof-tree} are great for \insist{learning} a system of logic, and for reasoning about general properties of the entire system. For example, the proof that \acrshort{ipl} is \vocab{internally-consistent} relies on a very careful argument about the structure of proof trees.

  Okay, but when we're actually \insist{proving} a single proposition, \vocabs{proof-tree} are really inconvenient, right? By this point in the course, you're almost certainly sick of writing proof trees by hand.

  The notation that we use for proof trees is a \insist{two-dimensional} notation: as we construct a tree bottom-up, it grows both \insist{taller} and \insist{wider}.

  In contrast, the notation of Agda is basically \insist{one-dimensional}. We're allowed to use newlines and indentation in a proof, but we're not required to: that's optional formatting for our own convenience.

  An Agda proof is much smaller than its corresponding proof tree because the Agda proof does not explicitly mention the judgements being proven at each step; it only explicitly mentions the judgement at the \insist{conclusion} of the whole proof.

  As we've seen, an Agda proof always contains enough information to ``reconstruct'' the corresponding proof tree on paper. In this sense, an Agda proof is just as \vocab{formal} as a proof tree is, even though the Agda proof contains less text.

  To be precise, we might say that Agda is a language of \vocabs{proof-term}, as opposed to \vocabs{proof-tree}. A \vocab{proof-term} is a \vocab{formal-proof} written as a string of text.

  The original goal of \vocab{lambda-calculus} was to be an effective notation for proof terms. This will be a guiding intuition behind our introduction to lambda calculus: we want a robust way to represent proof trees as one-dimensional strings of text.



\section{Simply-typed lambda calculus}


  There are many variants of \vocab{lambda-calculus}, including Agda itself. We'll begin our exploration with \vocabstyle{\acrfull{stlc}}, the oldest historical variant of lambda calculus that is a consistent \vocab{proof-system}.

  (There is a system called \vocab{untyped-lambda-calculus} which predates \acrshort{stlc}, but which ``failed'' at the original goal of lambda calculus: untyped lambda calculus can be seen today as a \vocab{programming-language} with \vocabs{dynamic-type}, but it is not a consistent \vocab{proof-system}.)

  In many ways \acrshort{stlc} is very similar to \acrshort{ipl}, but the change of proof system comes with a change of terminology. Most concepts in \acrshort{ipl} have different traditional names in the setting of \acrshort{stlc}.

  In our definition of \acrshort{ipl}, we defined \vocabs{proposition} and \vocabs{proof} as the core concepts of our proof system. In \acrshort{stlc}, \vocabs{type} take the place of \vocabs{proposition} and \vocabs{term} take the place of \vocabs{proof}.

  \begin{center}
    \begin{tabular}{| c | c |}
      \hline
      \acrshort{ipl} & \acrshort{stlc} \\
      \thickhline
      \vocab{proposition} & \vocab{type} \\
      \hline
      \vocab{proof} & \vocab{term} \\
      \hline
    \end{tabular}
  \end{center}


  \subsection{Variables}

    To define a notation for \vocabs{term} in \acrshort{stlc}, we will first need to define \vocabs{variable}. Variables are a new kind of thing in our system, not quite the same as \vocabs{metavariable} or \vocabs{constant}.

    In general, our variables will be written as strings of letters (which may include Greek letters). Variable names may include numbers, but we require that the \insist{first} character in a variable name must be a letter. We'll usually write variables with single-letter names like $x$ or $f_1$ but we'll also allow longer variable names like $number_{123}$ or $proof$.

    To be very precise in these notes, we will put lines over \vocabs{variable}, to distinguish them from \vocabs{constant} and \vocabs{metavariable}.
    \begin{itemize}
      \item \mono{P} and \mono{Q} are \vocabs{constant}
      \item \metavar{a} and \metavar{b} are \vocabs{metavariable}
      \item \var{x} and \var{y} are \vocabs{variable}
    \end{itemize}

    This is not a very common notation: in the ``real world'', we'd usually we'd just write $x$ and $y$ without the lines. The ``overline'' notation is just meant to remind us that \vocabs{variable} have their own special meaning in our system.


  \subsection{Terms}

    A \vocab{term} is a special kind of \vocab{expression}. The notation for \vocabs{term} in \acrshort{stlc} is a little more subtle than the notation for \vocabs{proposition} in \acrshort{ipl}, because of how \vocabs{variable} work in \acrshort{stlc}.

    We have several general ways to create \vocabs{term}:
    \begin{itemize}
      \item Every \vocab{variable} is a valid term by itself.
      \item Every \vocab{metavariable} is a valid term by itself.
      \item There is a two-position \vocab{operator}, written $\position{\cdot}\position$ and pronounced ``applied to''.
      \item For \insist{any} variable \var{var}, there is a one-position \vocab{operator} written $\lambda_{var}\position$ and pronounced ``lambda $var$'', or sometimes ``lambda $var$ returning''.
    \end{itemize}

    We will also introduce a handful of additional \vocabs{operator} when introducing the rules of \acrshort{stlc}.

    For example:
    \begin{itemize}
      \item $\var{x}$ \quad ``x''
      \item $\var{f} \cdot \var{x}$ \quad ``f applied to x''
      \item $\left(\var{f} \cdot \metavar{x}\right) \cdot \var{y}$ \quad ``(f applied to x) applied to y''
      \item $\var{f} \cdot \left(\var{x} \cdot \metavar{y}\right)$ \quad ``f applied to (x applied to y)''
      \item $\lambda_x\ \var{x}$ \quad ``lambda x x'' or ``lambda x returning x''
      \item $\lambda_f \left(\var{f} \cdot \var{x}\right)$ \quad ``lambda f returning (f applied to x)''
      \item $\lambda_f \left(\lambda_x \left(\var{f} \cdot \var{x}\right)\right)$ \quad ``lambda f returning (lambda x returning (f applied to x))''
    \end{itemize}

    Note that a $\lambda_{var}$ operator always has exactly \insist{one} child in a syntax tree, because it only has a single position: the $var$ is \insist{part of the \vocab{operator} name itself}, not a child of the operator. (This is also why we don't put a line over the $var$ in a $\lambda_{var}$ operator.)

    \begin{center}
      \example{
        term: $\left(\lambda_x \left(\lambda_y \left(\var{x} \cdot \var{y}\right)\right)\right) \cdot \lambda_z\ \var{z}$

        syntax tree:

        \begin{forest}
          [$\position{\cdot}\position$
            [$\lambda_x\position$
              [$\lambda_y\position$
                [$\position{\cdot}\position$
                  [$\var{x}$]
                  [$\var{y}$]
                ]
              ]
            ]
            [$\lambda_z\position$
              [$\var{z}$]
            ]
          ]
        \end{forest}
      }
    \end{center}

    In the rules for the operators of \acrshort{stlc}, we will also define some special \vocabs{constant} that can be used as terms.


  \subsection{Bindings and scopes}

    When we use a $\lambda_{var}\position$ operator, we say that the operator is a \vocab{lambda-binding} for the variable \var{var}. We might say that \var{var} is \vocab{lambda-bound}.
.
    The \vocab{scope} of a \vocab{lambda-binding} is the part of an \vocab{expression} where the lambda binding is ``active''. In general, the \vocab{scope} of a $\lambda_{var}\position$ operator includes \insist{everything below} the operator in a syntax tree, \insist{except} parts of the tree which are \insist{underneath another \vocab{lambda-binding}} for the \insist{same variable}.

    For example, consider the term $\left(\lambda_x \left(\left(\lambda_y \left(\var{x} \cdot \var{y}\right)\right) \cdot \left(\lambda_x\ \var{x}\right)\right)\right) \cdot \var{x}$. Note that there are \insist{two different lambda bindings} for \var{x} in this term. Here is its \vocab{syntax-tree}:

    \begin{center}
      \example{
        \begin{forest}
          [$\position{\cdot}\position$
            [$\lambda_x\position$
              [$\position{\cdot}\position$
                [$\lambda_y\position$
                  [$\position{\cdot}\position$
                    [$\var{x}$]
                    [$\var{y}$]
                  ]
                ]
                [$\lambda_x\position$
                  [$\var{x}$]
                ]
              ]
            ]
            [$\var{x}$]
          ]
        \end{forest}
      }
    \end{center}

    Here is the scope of the \insist{outermost} lambda binding for \var{x}, indicated in {\color{orange} orange} font.

    \begin{center}
      \example{
        term: $\left(\lambda_x \color{orange} \left(\left(\lambda_y \left(\var{x} \cdot \var{y}\right)\right) \cdot \left(\color{black} \lambda_x\ \var{x}\right)\right)\right) \cdot \var{x}$

        tree:

        \begin{forest}
          [$\position{\cdot}\position$
            [$\lambda_x\position$
              [\color{orange} $\position{\cdot}\position$,edge={orange}
                [\color{orange} $\lambda_y\position$,edge={orange}
                  [\color{orange} $\position{\cdot}\position$,edge={orange}
                    [\color{orange} $\var{x}$,edge={orange}]
                    [\color{orange} $\var{y}$,edge={orange}]
                  ]
                ]
                [$\lambda_x\position$,edge={orange}
                  [$\var{x}$]
                ]
              ]
            ]
            [$\var{x}$]
          ]
        \end{forest}
      }
    \end{center}

    Note that there are three uses of \var{x} in this term, and only one use is in the scope of the \insist{outermost} lambda binding for \var{x}. The second and third uses of \var{x} are in \insist{different scopes} than the first use: the second use is under a \insist{different} lambda binding for \var{x}, and the third use is under \insist{no} lambda binding for \var{x}.

    When a variable is used \insist{inside} the scope of a lambda binding, we say that the variable \insist{use} is \vocab{bound} by the binding node.

    In the example term above, reading left-to-right:
    \begin{itemize}
      \item The first use of \var{x} is lambda-bound by the outer $\lambda_x\position$ binding.
      \item The second use of \var{x} is lambda-bound by the inner $\lambda_x\position$ binding.
      \item The third use of \var{x} is not lambda-bound.
    \end{itemize}


  \subsection{Renaming}

    In any \vocab{term} with a \vocab{lambda-binding}, we are free to \vocab{rename} the \vocab{bound} variable as long as we also rename every \insist{use} of the variable \insist{in the scope of the lambda binding}.

    Consider the term $\left(\lambda_x \left(\left(\lambda_y \left(\var{x} \cdot \var{y}\right)\right) \cdot \left(\lambda_x\ \var{x}\right)\right)\right) \cdot \var{x}$ again. If we \vocab{rename} the \insist{outer} lambda binding for \var{x} to bind the variable \var{p} instead, we get $\left(\lambda_p \left(\left(\lambda_y \left(\var{p} \cdot \var{y}\right)\right) \cdot \left(\lambda_x\ \var{x}\right)\right)\right) \cdot \var{x}$.

    Note that only the first use of \var{x} changed, because it was the only use that was \vocab{bound} by the outer binder! Note also that \var{y} did not change, even though it is also in the scope of the outer binder for \var{x}.

    In general, whenever we \vocab{rename} a lambda binding, we must rename \insist{all and only} the uses of the \vocab{bound} variable that are in the scope of that lambda binding.

    If some term $t_1$ can be renamed to produce a different term $t_2$, we say that $t_1$ and $t_2$ are \vocab{alpha-equivalent} (pronounced ``alpha equivalent''). Formally, we write $t_1 \alphaequiv t_2$ to express that $t_1$ and $t_2$ are \vocab{alpha-equivalent}.

    For example, $\left(\lambda_x \left(\left(\lambda_y \left(\var{x} \cdot \var{y}\right)\right) \cdot \left(\lambda_x\ \var{x}\right)\right)\right) \cdot \var{x} \alphaequiv \left(\lambda_p \left(\left(\lambda_y \left(\var{p} \cdot \var{y}\right)\right) \cdot \left(\lambda_x\ \var{x}\right)\right)\right) \cdot \var{x}$.


  \subsection{Unification}

    In our entire \vocab{unification-algorithm}, we replace \vocab{definitional-equality} with \vocab{alpha-equivalence}.

    For example, consider this \vocab{unification-problem}:

    \begin{center}
      \example{
        $\left\{\begin{array}{r c l}
          \lambda_x \left(\var{x} \cdot \metavar{a}\right) & \unifyequals & \lambda_n \left(\var{n} \cdot \left(\var{p} \cdot \var{q}\right)\right)
        \end{array}\right\}$
      }
    \end{center}

    This problem \insist{does} have a \vocab{unifying-substitution}:

    \begin{center}
      \example{
        $\begin{cases}
          \metavar{a} & \mapsto\ \var{p} \cdot \var{q}
        \end{cases}$
      }
    \end{center}

    This solution is valid because $\lambda_x \left(\var{x} \cdot \left(\var{p} \cdot \var{q}\right)\right) \alphaequiv \lambda_n \left(\var{n} \cdot \left(\var{p} \cdot \var{q}\right)\right)$.

    This will affect the meanings of all of our \vocabs{inference-rule}: when an inference rule mentions a term with a binder, it is applicable to \insist{any} \vocab{alpha-equivalent} term, not just terms where the corresponding binder uses the exact same variable name.



  \subsection{Types}

    We traditionally use different \vocabs{operator-name} in \acrshort{stlc} than in \acrshort{ipl}. In general, we have a \vocab{type} operator in \acrshort{stlc} for each \vocab{proposition} operator in \acrshort{ipl}.

    \begin{center}
      \begin{tabular}{| c | c |}
        \hline
        \acrshort{ipl} & \acrshort{stlc} \\
        \thickhline
        $\position{\wedge}\position$ & $\position{\times}\position$ \\
        \hline
        $\position{\vee}\position$ & $\position{\uplus}\position$ \\
        \hline
        $\position{\subset}\position$ & $\position{\to}\position$ \\
        \hline
        $\top$ & $\mono{Unit}$ \\
        \hline
        $\bot$ & $\mono{Void}$ \\
        \hline
      \end{tabular}
    \end{center}

    (Note that this \mono{Void} type is unrelated to the \mono{void} type in C-like languages!)

    A \vocab{type} is an \vocab{expression} using any of these \vocabs{operator}, just like how we defined \vocabs{proposition} in \acrshort{ipl}. For now, we will also allow \vocabs{type} to include \vocabs{arbitrary-constant} like \mono{P} and \mono{Q}.


  \subsection{Contexts}

    Our formal rules of \acrshort{stlc} will still use \vocab{contexts}, but instead of \vocab{propositions}, our contexts will contain another form of variable binding.

    We introduce an operator written $\position{:}\position$ and pronounced ``has type'' or ``is of type'':
    \begin{itemize}
      \item $x : t$ is pronounced ``x has type t'' or ``x is of type t''
    \end{itemize}

    The $\position{:}\position$ operator has a totally different meaning than our existing $\position{\cons}\position$ operator: it's just a historical coincidence that they look confusingly similar.

    Each element in an \acrshort{stlc} \vocab{context} is a \vocab{expression} with the $\position{:}\position$ operator at the root of its \vocab{syntax-tree}, with a single \vocab{variable} name on the left and any \vocab{type} on the right.

    We will use the term \vocab{local-binding} for these expressions. If we can prove $\left(x : t\right) \in D$ for some \vocab{variable} name $x$, \vocab{type} $t$, and \vocab{context} $D$, then we will say $x$ is \vocab{locally-bound} in $D$ with the type $t$.

    Here are some example \vocabs{context} in \acrshort{stlc}:
    \begin{itemize}
      \item $\emptyset$
      \item $\left(x : \mono{Void}\right) \cons \emptyset$
      \item $\left(y : \mono{Unit} \uplus \mono{Void}\right) \cons \emptyset$
      \item $\left(a_1 : \mono{P} \times \mono{Q}\right) \cons \left(a_2 : \mono{Unit} \uplus \mono{Void}\right) \cons \emptyset$
    \end{itemize}


  \subsection{Typing judgements}

    Ultimately, a \vocab{term} in \acrshort{stlc} is a \vocab{proof} by itself, so when we \insist{use} \acrshort{stlc} to write actual proofs, we're usually \insist{not} using a \vocab{proof-tree} notation.

    To \insist{define} \acrshort{stlc} and reason about its definition, we traditionally \insist{do} use a \vocab{proof-tree} notation. This allows us to formally and visually ``connect'' the worlds of \vocabs{proof-tree} and \vocabs{proof-term}, in order to specify and examine exactly \insist{how} the two concepts relate to each other.

    Instead of \vocabs{entailment-judgement}, our proof trees in \acrshort{stlc} will generally contain \vocabs{typing-judgement}. A \vocab{typing-judgement} is an \vocab{expression} with the \insist{three-position} operator $\position{\entails}\position{:}\position$ at the root of its \vocab{syntax-tree}.

    The $\position{\entails}\position{:}\position$ operator is always used with a \vocab{context} on the left, a \vocab{term} in the middle, and a \vocab{type} on the right. For some context $D$, term $e$, and type $t$, we pronounce $D \entails e : t$ as ``$D$ entails $e$ has type $t$'', or ``$e$ has type $t$ in context $D$''.

    Here are some examples of \vocabs{typing-judgement}:
    \begin{itemize}
      \item $\emptyset \entails \var{x} : \mono{Unit}$
      \item $\emptyset \entails \var{x} : \mono{P}$
      \item $\left(\var{x} : \mono{Void}\right) \cons \emptyset \entails \var{x} \cdot \var{y} : \mono{P} \times \mono{Q}$
      \item $\left(a_1 : \mono{P} \times \mono{Q}\right) \cons \left(a_2 : \mono{Unit} \uplus \mono{Void}\right) \cons \emptyset \entails \var{x} \cdot \var{y} : \left(\mono{P} \uplus \mono{Void}\right) \to \mono{Q}$
    \end{itemize}

    Typing judgements can get very dense, so make sure to take your time reading them carefully!

\input{include/footer.tex}
\end{document}
