\input{include/header.tex}

% auto-generated glossary terms
\input{notes/6.glt}

% rules of inference
\input{include/theories/iplc.tex}


\indextitle{Implication and disjunction}
\subtitle{Proofs about conditionals}

\makeglossaries


\begin{document}

\maketitle
\tableofcontents

\newpage


\section{Introduction}


  Now that we have a properly formal way to write our \assumed\ rule with a \vocab{context} of \vocabs{assumption}, we can finally introduce the rest of the \vocabs{rule-of-inference} that define \vocabstyle{\acrfull{ipl}}. This will involve slight modifications to the rules that we've already seen, along with the addition of a few new rules.

  These notes will cover the entire \vocab{positive-fragment} of \acrshort{ipl}, meaning we'll be able to prove everything that ``should'' be provable in \acrshort{ipl}. The topic we have to introduce is \vocab{negation}, which will allow us to \vocab{disprove} everything that ``should'' be disprovable in \acrshort{ipl}. With the rules for negation, we will have a complete definition of \acrshort{ipl}, and we'll be able to give a complete definition of \vocab{classical-propositional-logic} as well.

  In previous notes, we mentioned that the rules for a logical operator generally come in complementary sets of \vocabs{introduction-rule} (-\rulename{Intro}) and \vocabs{elimination-rule} (-\rulename{Elim}). Intuitively, introduction rules allow us to ``decompose'' a \insist{problem} into smaller subproblems, and elimination rules allow us to ``deconstruct'' an \insist{assumption} into smaller ``sub-assumptions''.

  (The \assumed\ rule is neither an introduction rule nor an elimination rule, because it doesn't directly involve any of the \acrshort{ipl} operators, just the $\in$ and $\entails$ judgements. It's kind of like an elimination rule for the $\position{\in}\position$ operator, but it has somewhat different logical properties than ``real'' elimination rules.)

  Our fragment of \acrshort{ipl} had both of these capabilities for reasoning about \vocab{conjunction} (the $\position{\wedge}\position$ operator):
  \begin{itemize}
    \item We can use \andIntro\ to ``decompose'' the problem of proving $\mono{P} \wedge \mono{Q}$ into the smaller subproblems of proving \mono{P} and proving \mono{Q}.
    \item We can use \andElimL\ and \andElimR\ to ``deconstruct'' an assumption $\mono{P} \wedge \mono{Q}$ into the smaller ``sub-assumptions'' \mono{P} and \mono{Q}.
  \end{itemize}

  In contrast, we were missing some capabilities for the $\position{\vee}\position$ and $\position{\subset}\position$ operators:
  \begin{itemize}
    \item We can use \orIntroL\ or \orIntroR\ to ``decompose'' the problem of proving $\mono{P} \vee \mono{Q}$ into the smaller subproblem of proving \mono{P} or proving \mono{Q}, but we had no way to ``deconstruct'' an assumption $\mono{P} \vee \mono{Q}$.
    \item We can use \implElim\ to ``deconstruct'' an assumption $\mono{P} \subset \mono{Q}$ and a proof of \mono{P} into the smaller ``sub-assumption'' \mono{Q}, but we had no way to ``decompose'' the problem of proving $\mono{P} \subset \mono{Q}$.
  \end{itemize}

  We technically could introduce a $\operatorname{{\top}-\rulename{Elim}}$ rule to complement our \topIntro\ rule, but it wouldn't allow us to prove anything new, so we won't bother with it.

  \longdemo{Why would a $\operatorname{{\top}-\rulename{Elim}}$ rule be pointless? What would it look like if we defined it anyway?}

  The rules that were missing from our previous fragment of \acrshort{ipl} are \implIntro\ and \orElim. In these notes, we'll see how these rules are defined, how they relate to each other, and how they both relate to the concept of a \vocab{conditional}.



\section{Conjunction}


  First, let's briefly review the rules for \vocab{conjunction}, and then go a bit deeper into how its \vocab{introduction-rule} and \vocab{elimination-rules} relate to each other. This will help build some intuition for what we should expect the definitions of \implIntro\ and \orElim\ to look like.


  \subsection{Rules}

    Here are the upgraded rules for $\wedge$ from the notes on structural logic:

    \begin{center}
      \AndIntro

      \AndElimL \quad \AndElimR
    \end{center}

    Now that we have contexts in our proofs, we can actually define an alternative \andElim\ rule that covers the use cases of both \andElimL\ and \andElimR\ at the same time.

    \begin{center}
      \AndElim
    \end{center}

    Read left-to-right and top-down, we might say:
    \begin{itemize}
      \item if we can prove \metavar{a} and \metavar{b} assuming only the propositions in $\metavar{\Gamma}$,
      \item and we can prove \metavar{c} assuming \metavar{a} and \metavar{b} along with the propositions in $\metavar{\Gamma}$,
      \item then we can prove \metavar{c} assuming only the propositions in $\metavar{\Gamma}$.
    \end{itemize}

    In a proof using \andElim, the right branch can use \assumed\ with \inAt{0}\ in place of \andElimL\ and with \inAt{1}\ in place of \andElimR.

    \begin{center}
      \example{\center
              \AxiomC{\assumption{}}
            \LeftLabel{$SomeRuleX$}
            \UnaryInfC{$\metavar{\Gamma} \entails \metavar{a} \wedge \metavar{b}$}
          \LeftLabel{\andElimL}
          \UnaryInfC{$\metavar{\Gamma} \entails \metavar{a}$}
        \LeftLabel{$SomeRuleY$}
        \UnaryInfC{\assumption{}}
        \DisplayProof

        \bigskip

        is equivalent to

        \bigskip

              \AxiomC{\assumption{}}
            \LeftLabel{$SomeRuleX$}
            \UnaryInfC{$\metavar{\Gamma} \entails \metavar{a} \wedge \metavar{b}$}
                \AxiomC{}
              \LeftLabel{\inAt{0}}
              \UnaryInfC{$\metavar{a} \in \metavar{a} \cons \metavar{b} \cons \metavar{\Gamma}$}
            \LeftLabel{\assumed}
            \UnaryInfC{$\metavar{a} \cons \metavar{b} \cons \metavar{\Gamma} \entails \metavar{a}$}
          \LeftLabel{\andElim}
          \BinaryInfC{$\metavar{\Gamma} \entails \metavar{a}$}
        \LeftLabel{$SomeRuleY$}
        \UnaryInfC{\assumption{}}
        \DisplayProof
      }

      \example{\center
              \AxiomC{\assumption{}}
            \LeftLabel{$SomeRuleX$}
            \UnaryInfC{$\metavar{\Gamma} \entails \metavar{a} \wedge \metavar{b}$}
          \LeftLabel{\andElimR}
          \UnaryInfC{$\metavar{\Gamma} \entails \metavar{a}$}
        \LeftLabel{$SomeRuleY$}
        \UnaryInfC{\assumption{}}
        \DisplayProof

        \bigskip

        is equivalent to

        \bigskip

              \AxiomC{\assumption{}}
            \LeftLabel{$SomeRuleX$}
            \UnaryInfC{$\metavar{\Gamma} \entails \metavar{a} \wedge \metavar{b}$}
                \AxiomC{}
              \LeftLabel{\inAt{1}}
              \UnaryInfC{$\metavar{b} \in \metavar{a} \cons \metavar{b} \cons \metavar{\Gamma}$}
            \LeftLabel{\assumed}
            \UnaryInfC{$\metavar{a} \cons \metavar{b} \cons \metavar{\Gamma} \entails \metavar{b}$}
          \LeftLabel{\andElim}
          \BinaryInfC{$\metavar{\Gamma} \entails \metavar{b}$}
        \LeftLabel{$SomeRuleY$}
        \UnaryInfC{\assumption{}}
        \DisplayProof
      }
    \end{center}

    Intuitively, this alternative \andElim\ rule lets us ``unpack'' a proof of a conjunction into two assumptions that we get to use in the right branch. Whatever proposition we prove in the right branch is the conclusion of the whole \andElim\ subtree, but without the extra assumptions in the context because we \vocablink{satisfy}{satisfied} them with the proof in the left branch.

    \andElim\ is fully equivalent in ``power'' to the pair of \andElimL\ and \andElimR: any proof using \andElimL\ and \andElimR\ can be rewritten to use only \andElim, and vice versa. This means we can define \andElim\ as a \vocab{derived-rule} if we have \andElimL\ and \andElimR\ as \vocabs{inference-rule}, or we can define \andElimL\ and \andElimR\ as \vocabs{derived-rule} if we have \andElim\ as an \vocab{inference-rule}.

    In practice, this choice doesn't affect our efforts constructing proof trees, because \andElimL, \andElimR, and \andElim\ are all valid to use in our proof trees either way. \andElim\ can be more convenient when we want to use both parts of a conjunction; \andElimL\ and \andElimR\ are more convenient when we want to ``throw away'' one part and just use the other part.

    (The difference between an inference rule and a derived rule only comes up when we're trying to prove something about our entire \vocab{proof-system}. In that setting, we get to ``ignore'' derived rules and focus only on proving something about each of the inference rules that define the proof system.)


  \subsection{Detours}

    When constructing a proof tree, it's possible to take \vocabs{detour} that make the proof larger without making any actual progress. These kinds of detours do not invalidate a proof, but it can still be very helpful to study when and why they might happen and what can be done to avoid them. In a sense, an algorithm for ``removing detours'' will be the eventual definition of \vocab{computation} in our proof system.

    With the $\wedge$ operator, we can introduce a detour when we use \andElimL\ or \andElimR\ with a premise that we prove with \andIntro. For a minimal example:

    \begin{center}
      \example{
                \AxiomC{}
              \LeftLabel{\inAt{0}}
              \UnaryInfC{$\mono{P} \in \mono{P} \cons \emptyset$}
            \LeftLabel{\assumed}
            \UnaryInfC{$\mono{P} \cons \emptyset \entails \mono{P}$}
              \AxiomC{}
            \RightLabel{\topIntro}
            \UnaryInfC{$\mono{P} \cons \emptyset \entails \top$}
          \LeftLabel{\andIntro}
          \BinaryInfC{$\mono{P} \cons \emptyset \entails \mono{P} \wedge \top$}
        \LeftLabel{\andElimL}
        \UnaryInfC{$\mono{P} \cons \emptyset \entails \mono{P}$}
        \DisplayProof
      }
    \end{center}

    This proof is valid, but the right branch is unnecessary: the left branch already proves the exact judgement that we're trying to conclude. We could have just used the left branch starting with \assumed\ as our entire proof!

    In general, whenever \andElimL\ or \andElimR\ follows \andIntro\ (reading top-down), the detour can be avoided by rewriting the proof. Intuitively, we just ``throw away'' whichever branch isn't used in the rest of the proof (still reading top-down).

    \begin{center}
      \example{
                \AxiomC{\assumption{}}
              \LeftLabel{$SomeRuleX$}
              \UnaryInfC{$\metavar{\Gamma} \entails \metavar{a}$}
                \AxiomC{\assumption{}}
              \RightLabel{$SomeRuleY$}
              \UnaryInfC{$\metavar{\Gamma} \entails \metavar{b}$}
            \LeftLabel{\andIntro}
            \BinaryInfC{$\metavar{\Gamma} \entails \metavar{a} \wedge \metavar{b}$}
          \LeftLabel{\andElimL}
          \UnaryInfC{$\metavar{\Gamma} \entails \metavar{a}$}
        \LeftLabel{$SomeRuleZ$}
        \UnaryInfC{\assumption{}}
        \DisplayProof
        \quad rewrites to \quad
          \AxiomC{\assumption{}}
          \RightLabel{$SomeRuleX$}
          \UnaryInfC{$\metavar{\Gamma} \entails \metavar{a}$}
        \RightLabel{$SomeRuleZ$}
        \UnaryInfC{\assumption{}}
        \DisplayProof
      }

      \example{
                \AxiomC{\assumption{}}
              \LeftLabel{$SomeRuleX$}
              \UnaryInfC{$\metavar{\Gamma} \entails \metavar{a}$}
                \AxiomC{\assumption{}}
              \RightLabel{$SomeRuleY$}
              \UnaryInfC{$\metavar{\Gamma} \entails \metavar{b}$}
            \LeftLabel{\andIntro}
            \BinaryInfC{$\metavar{\Gamma} \entails \metavar{a} \wedge \metavar{b}$}
          \LeftLabel{\andElimR}
          \UnaryInfC{$\metavar{\Gamma} \entails \metavar{a}$}
        \LeftLabel{$SomeRuleZ$}
        \UnaryInfC{\assumption{}}
        \DisplayProof
        \quad rewrites to \quad
          \AxiomC{\assumption{}}
          \RightLabel{$SomeRuleY$}
          \UnaryInfC{$\metavar{\Gamma} \entails \metavar{b}$}
        \RightLabel{$SomeRuleZ$}
        \UnaryInfC{\assumption{}}
        \DisplayProof
      }
    \end{center}

    Together, these \vocab{rewrite-rules} can be seen as justification that \andElimL\ and \andElimR\ are a ``correct'' set of elimination rules to pair with \andIntro. This justification applies in general: the introduction rules and elimination rules for an operator are always related by the kinds of detours that they allow when used together.

    With introduction and elimination rules, we expect the intuitive property that ``you get out exactly what you put in'': for the $\wedge$ operator, any proof of a conjunction $P \wedge Q$ should ``contain'' \insist{exactly} a proof of $P$, a proof of $Q$, and nothing else. This is why it's a detour if we introduce a conjunction only to eliminate it: we can't ``get out'' anything from the elimination rule except what we ``put in'' with the introduction rule.

    \longdemo{How does this apply to the alternative \andElim\ rule?}



\section{Implication}


  \subsection{Intuition}

    The \vocablink{sym-subset}{$\position{\subset}\position$} operator in our definition of \acrshort{ipl} corresponds to logical \vocab{implication}, which is \insist{one} kind of \vocab{conditional} (a statement of the form ``if\ldots then\ldots'').

    As we've seen with the \satisfy\ rule, the $\position{\entails}\position$ operator is \insist{also} a kind of conditional: \insist{if} we have proofs of the propositions on the left, \insist{then} we can prove the proposition on the right.

    Intuitively, there is a close connection between the $\subset$ operator and the $\entails$ operator, but there is also a meaningful difference between them: the $\subset$ operator represents a conditional ``inside'' \acrshort{ipl}, and the $\entails$ operator represents a conditional ``outside'' \acrshort{ipl} (in our logic of contexts and assumptions).

    \begin{itemize}
      \item $\mono{P} \cons \emptyset \entails \mono{Q}$ means ``if I have a proof of \mono{P}, I can construct a proof of \mono{Q}''.
      \item $\emptyset \entails \mono{P} \subset \mono{Q}$ means ``I can construct a proof of `if \mono{P} then \mono{Q}' (a tautology)''.
      \item $\mono{P} \cons \emptyset \entails \mono{Q} \subset \mono{R}$ means ``if I have a proof of \mono{P}, I can construct a proof of `if \mono{Q} then \mono{R}'\thinspace''.
    \end{itemize}

    In a sense, the main purpose of the $\subset$ operator is to give us a way to prove tautologies about conditionals. Without the $\subset$ operator, the only way we had to represent any form of conditional was by listing assumptions to the left of the $\entails$ in an entailment judgement. By definition, we can't prove a tautology that way.

    It might sound like a contradiction to have a ``tautology about a conditional'', because a tautology is ``always true under any condition''. It's very subtle, but the trick is that we're proving a ``tautology about a conditional'', not a ``conditional tautology''.

    To see this difference intuitively, consider a C-like language with a \mono{\position{?}\position{:}\position} Boolean ``conditional'' operator (ignoring the possibility of ``undefined behavior''):
    \begin{itemize}
      \item It is ``always true under any condition'' that \mbox{\mono{x ? 1 : 1}} evaluates to \mono{1}.
      \item It is only ``true under some condition'' that \mono{x ? 1 : 2} evaluates to \mono{1}.
    \end{itemize}

    More technically, as we'll see in later notes, the relationship between $\subset$ and $\entails$ is like the relationship between \vocabs{bound-variable} and \vocabs{free-variable} in the syntax of a programming language. We might think of propositions to the left of a $\subset$ as ``inputs'' to our proof which have not been bound to ``variables'' yet, and then assumptions to the left of the $\entails$ are ``variables'' that have already been bound.


  \subsection{Rules}

    Here is our new \vocab{introduction-rule} for the implication operator, along with the upgraded \vocab{elimination-rule} from the previous notes on structural logic.

    \begin{center}
      \ImplIntro \quad \ImplElim
    \end{center}

    Note carefully that the \implIntro\ rule has a \insist{different} context in its \vocab{premise} than in its \vocab{conclusion}. Unlike our \andElim\ variants, we have no alternative way to formally define \implIntro\ without \vocabs{context}.

    Focusing just on the notation, we might say that the \implIntro\ rule allows us to ``move'' a proposition between the left of a $\subset$ and the left of the $\entails$. Intuitively, the \implIntro\ rule relates the concepts of \vocabs{assumption} and \vocabs{conditional}.

    \begin{itemize}
      \item Reading bottom-up: ``to prove `if $\metavar{a}$ then $\metavar{b}$', we can prove $\metavar{b}$ using $\metavar{a}$ as an assumption''.
      \item Reading top-down: ``if we have a proof of $\metavar{b}$ using $\metavar{a}$ as an assumption, we can prove `if $\metavar{a}$ then $\metavar{b}$' without using $\metavar{a}$ as an assumption''.
    \end{itemize}

    Reading \implIntro\ bottom-up, note that the proposition on the right of the $\entails$ gets \insist{smaller}: specifically, for any propositions $P$ and $Q$, the proposition $P \subset Q$ has more nodes in its syntax tree than $Q$ has by itself. This is how \implIntro\ allows us to ``break down the problem'' of proving an implication into a smaller subproblem.

    The context on the left of the $\entails$ gets larger as we construct bottom-up with \implIntro, but this makes our problem \insist{easier} to solve: it means there are more things we get to assume as we continue to construct this branch of our proof. The proposition on the right of the $\entails$ \insist{is} the problem that we're trying to solve: by ``breaking down'' the proposition and growing our context, we're making progress on our solution.


  \subsection{Detours}

    When we use \implIntro\ and \implElim\ together, we can introduce detours that relate to the \satisfy\ \vocab{derived-rule} from the notes on structural logic. For a minimal example:

    \begin{center}
      \example{
                \AxiomC{}
              \LeftLabel{\inAt{1}}
              \UnaryInfC{$\mono{P} \in \top \cons \mono{P} \cons \emptyset$}
            \LeftLabel{\assumed}
            \UnaryInfC{$\top \cons \mono{P} \cons \emptyset \entails \mono{P}$}
          \LeftLabel{\implIntro}
          \UnaryInfC{$\mono{P} \cons \emptyset \entails \top \subset \mono{P}$}
            \AxiomC{}
          \RightLabel{\topIntro}
          \UnaryInfC{$\mono{P} \cons \emptyset \entails \top$}
        \LeftLabel{\implElim}
        \BinaryInfC{$\mono{P} \cons \emptyset \entails \mono{P}$}
        \DisplayProof
      }
    \end{center}

    In this example, the \implIntro\ and \implElim\ rules can be avoided by rewriting the proof from scratch and using the \assumed\ rule from the start.

    \begin{center}
      \example{
            \AxiomC{}
          \LeftLabel{\inAt{0}}
          \UnaryInfC{$\mono{P} \in \mono{P} \cons \emptyset$}
        \LeftLabel{\assumed}
        \UnaryInfC{$\mono{P} \cons \emptyset \entails \mono{P}$}
        \DisplayProof
      }
    \end{center}

    For this minimal example it's straightforward to rewrite the proof from scratch, but it won't be straightforward with more complex proofs. What if we wanted to systematically ``combine'' our two original branches into a valid proof, without erasing all of the work we had done so far? 

    \begin{center}
      \example{
            \AxiomC{}
          \LeftLabel{\inAt{1}}
          \UnaryInfC{$\mono{P} \in \top \cons \mono{P} \cons \emptyset$}
        \LeftLabel{\assumed}
        \UnaryInfC{$\top \cons \mono{P} \cons \emptyset \entails \mono{P}$}
        \DisplayProof
        \quad
          \AxiomC{}
        \RightLabel{\topIntro}
        \UnaryInfC{$\mono{P} \cons \emptyset \entails \top$}
        \DisplayProof
      }
    \end{center}

    This is exactly what our derived \satisfy\ rule accomplishes, we just have to swap the left-to-right order of the branches. (We could have defined our \satisfy\ rule with the branches in the other order and avoided swapping them here, it's just traditional to put the premise with the smaller context on the left in the definition of \satisfy.)

    \begin{center}
      \example{
          \AxiomC{}
        \LeftLabel{\topIntro}
        \UnaryInfC{$\mono{P} \cons \emptyset \entails \top$}
            \AxiomC{}
          \RightLabel{\inAt{1}}
          \UnaryInfC{$\mono{P} \in \top \cons \mono{P} \cons \emptyset$}
        \RightLabel{\assumed}
        \UnaryInfC{$\top \cons \mono{P} \cons \emptyset \entails \mono{P}$}
        \LeftLabel{\satisfy}
        \BinaryInfC{$\mono{P} \cons \emptyset \entails \mono{P}$}
        \DisplayProof
      }
    \end{center}

    Since \satisfy\ is a \vocab{derived-rule}, we know that we \insist{could} convert this proof into one that doesn't use \satisfy, if we wanted to. It's often more important to know that this is possible than to actually do it. This conversion would involve manually ``plugging'' the left branch of \satisfy\ into the right branch, like we discussed informally in the notes on structural logic.

    If we did the conversion to remove \satisfy\ from the above proof, we would end up with the same simple proof that we could have produced by rewriting from scratch.

    \begin{center}
      \example{
            \AxiomC{}
          \LeftLabel{\inAt{0}}
          \UnaryInfC{$\mono{P} \in \mono{P} \cons \emptyset$}
        \LeftLabel{\assumed}
        \UnaryInfC{$\mono{P} \cons \emptyset \entails \mono{P}$}
        \DisplayProof
      }
    \end{center}

    In general, whenever \implElim\ follows \implIntro\ (reading top-down), the detour can be avoided by rewriting with \satisfy. If we want to simplify the proof further, we can then use the ``plugging in'' technique to remove the \satisfy\ rule from the proof.

    \begin{center}
      \example{\center
                \AxiomC{\assumption{}}
              \LeftLabel{$SomeRuleX$}
              \UnaryInfC{$\metavar{a} \cons \metavar{\Gamma} \entails \metavar{b}$}
            \LeftLabel{\implIntro}
            \UnaryInfC{$\metavar{\Gamma} \entails \metavar{a} \subset \metavar{b}$}
              \AxiomC{\assumption{}}
            \RightLabel{$SomeRuleY$}
            \UnaryInfC{$\metavar{\Gamma} \entails \metavar{a}$}
          \LeftLabel{\implElim}
          \BinaryInfC{$\metavar{\Gamma} \entails \metavar{b}$}
        \LeftLabel{$SomeRuleZ$}
        \UnaryInfC{\assumption{}}
        \DisplayProof

        \bigskip

        rewrites to

        \bigskip

              \AxiomC{\assumption{}}
            \LeftLabel{$SomeRuleY$}
            \UnaryInfC{$\metavar{\Gamma} \entails \metavar{a}$}
              \AxiomC{\assumption{}}
            \RightLabel{$SomeRuleX$}
            \UnaryInfC{$\metavar{a} \cons \metavar{\Gamma} \entails \metavar{b}$}
          \RightLabel{\satisfy}
          \BinaryInfC{$\metavar{b}$}
        \LeftLabel{$SomeRuleZ$}
        \UnaryInfC{\assumption{}}
        \DisplayProof
      }
    \end{center}



\section{Disjunction}


  \subsection{Intuition}

    The \vocablink{sym-vee}{$\position{\vee}\position$} operator in our definition of \acrshort{ipl} corresponds to logical \vocab{disjunction}, which can be seen as another kind of \vocab{conditional} in the form ``either\ldots or\ldots''.

    The $\vee$ operator is very different than the $\wedge$ operator, even though they share many mathematical properties. This is kind of like the relationship between addition and multiplication: they share properties like $x + y = y + x$ and $x * y = y * x$, but $3 + 4$ is a completely different number than $3 * 4$.

    Intuitively, in the same sense that a proof of $P \wedge Q$ ``contains'' exactly a proof of $P$ \insist{and} a proof of $Q$, we have that a proof of $P \vee Q$ ``contains'' exactly a proof of $P$ \insist{or} a proof of $Q$. We can ``extract'' this ``contained'' proof, but with a catch: we must say what to do in \insist{both} cases in order for our proof to be valid.

    Logically, the $\vee$ operator is an ``inclusive OR'': it is possible that both $P \wedge Q$ and $P \vee Q$ might be true for the same propositions $P$ and $Q$. In a single proof tree, though, a proof of $P \vee Q$ contains \insist{either} one proof of $P$ \insist{or} one proof of $Q$, not both at the same time.

    (To define an ``exclusive OR'' operator, we'd usually use \vocab{negation}, which we haven't defined yet. It's common to use the $\veebar$ symbol to represent the ``exclusive'' variant of the $\vee$ symbol.)


  \subsection{Rules}

    Here is our new \vocab{elimination-rule} for the disjunction operator, along with the upgraded \vocabs{introduction-rule} from the previous notes on structural logic.

    \begin{center}
      \OrIntroL \quad \OrIntroR

      \OrElim
    \end{center}

    \longdemo{Could we merge the \orIntroL\ and \orIntroR\ rules into a single introduction rule, like we did with the \andElimL\ and \andElimR\ rules? Could we split \orElim\ into two elimination rules?}

    Note that \orElim\ is our first rule with \insist{three} premises. The first premise is the disjunction we want to eliminate; the second and third premises are the \vocabs{case} that we need to prove in order to eliminate the disjunction.

    In general, if we have a proof of $\metavar{\Gamma} \entails \metavar{a} \vee \metavar{b}$, we can ``extract'' a proof of \metavar{a} or a proof of \metavar{b}, but we don't always know which one it will be. If we have some proposition $P \vee Q$ as an \vocab{assumption}, we know that it ``contains'' either a proof of $P$ or a proof of $Q$, but we may have no way to know exactly which one it ``contains''.

    This is why \orElim\ requires us to account for both cases: in the second and third premise we must provide \insist{two} proofs that each use \insist{one} additional assumption, and both must prove the same proposition. Reading top-down, if we can prove \metavar{c} assuming \metavar{a} \insist{and} we can prove \metavar{c} assuming \metavar{b}, then we can prove \metavar{c} assuming $\metavar{a} \vee \metavar{b}$.

    Reading left-to-right and bottom-up, \orElim\ uses a proof of $\metavar{a} \vee \metavar{b}$ in the first branch to ``split'' the overall proof into two new branches, one that gets to use \metavar{a} and one that gets to use \metavar{b}. Neither branch gets to use both assumptions, because the proof of $\metavar{a} \vee \metavar{b}$ only contains one proof or the other.


  \subsection{Detours}

    When we use \orIntroL\ or \orIntroR\ along with \orElim, we can introduce detours that leave ``dead code'' in our proofs. For a minimal example:

    \begin{center}
      \example{
                \AxiomC{}
              \LeftLabel{\inAt{0}}
              \UnaryInfC{$\mono{P} \in \mono{P} \cons \emptyset$}
            \LeftLabel{\assumed}
            \UnaryInfC{$\mono{P} \cons \emptyset \entails \mono{P}$}
          \LeftLabel{\orIntroL}
          \UnaryInfC{$\mono{P} \cons \emptyset \entails \mono{P} \vee \top$}
              \AxiomC{}
            \RightLabel{\inAt{0}}
            \UnaryInfC{$\mono{P} \in \mono{P} \cons \mono{P} \cons \emptyset$}
          \RightLabel{\assumed}
          \UnaryInfC{$\mono{P} \cons \mono{P} \cons \emptyset \entails \mono{P}$}
            \AxiomC{}
          \RightLabel{\topIntro}
          \UnaryInfC{$\mono{P} \cons \emptyset \entails \top$}
        \LeftLabel{\orElim}
        \TrinaryInfC{$\mono{P} \cons \emptyset \entails \mono{P}$}
        \DisplayProof
      }
    \end{center}

    Intuitively, there's no need for \orElim\ in this proof because we \insist{know} that our subproof of $\mono{P} \vee \top$ actually contains a proof of \mono{P}: we constructed it ourselves with \orIntroL.

    Like with the implication detour example, it's not hard to see how to rewrite this proof from scratch, but what if we wanted to systematically ``combine'' the branches of the proof into a new valid proof without \orIntroL\ and \orElim?

    We can just ``throw away'' whichever branch is irrelevant: if we used \orIntroL\ then we need the first and second branch, and if we used \orIntroR\ then we need the first and third branch.

    \begin{center}
      \example{
            \AxiomC{}
          \LeftLabel{\inAt{0}}
          \UnaryInfC{$\mono{P} \in \mono{P} \cons \emptyset$}
        \LeftLabel{\assumed}
        \UnaryInfC{$\mono{P} \cons \emptyset \entails \mono{P}$}
        \DisplayProof
        \quad
            \AxiomC{}
          \RightLabel{\inAt{0}}
          \UnaryInfC{$\mono{P} \in \mono{P} \cons \mono{P} \cons \emptyset$}
        \RightLabel{\assumed}
        \UnaryInfC{$\mono{P} \cons \mono{P} \cons \emptyset \entails \mono{P}$}
        \DisplayProof
      }
    \end{center}

    Now we can simply join these two branches with \satisfy:

    \begin{center}
      \example{
              \AxiomC{}
            \LeftLabel{\inAt{0}}
            \UnaryInfC{$\mono{P} \in \mono{P} \cons \emptyset$}
          \LeftLabel{\assumed}
          \UnaryInfC{$\mono{P} \cons \emptyset \entails \mono{P}$}
              \AxiomC{}
            \RightLabel{\inAt{0}}
            \UnaryInfC{$\mono{P} \in \mono{P} \cons \mono{P} \cons \emptyset$}
          \RightLabel{\assumed}
          \UnaryInfC{$\mono{P} \cons \mono{P} \cons \emptyset \entails \mono{P}$}
        \LeftLabel{\satisfy}
        \BinaryInfC{$\mono{P} \cons \emptyset \entails \mono{P}$}
        \DisplayProof
      }
    \end{center}

    In general, whenever \orElim\ follows \orIntroL\ or \orIntroR\ (reading top-down), the detour can be avoided by ``throwing away'' the irrelevant branch and using \satisfy\ to join the other two branches.

    \begin{center}
      \example{\center
                \AxiomC{\assumption{}}
              \LeftLabel{$SomeRuleW$}
              \UnaryInfC{$\metavar{\Gamma} \entails \metavar{a}$}
            \LeftLabel{\orIntroL}
            \UnaryInfC{$\metavar{\Gamma} \entails \metavar{a} \vee \metavar{b}$}
              \AxiomC{\assumption{}}
            \RightLabel{$SomeRuleX$}
            \UnaryInfC{$\metavar{a} \cons \metavar{\Gamma} \entails \metavar{c}$}
              \AxiomC{\assumption{}}
            \RightLabel{$SomeRuleY$}
            \UnaryInfC{$\metavar{b} \cons \metavar{\Gamma} \entails \metavar{c}$}
          \LeftLabel{\orElim}
          \TrinaryInfC{$\metavar{\Gamma} \entails \metavar{c}$}
        \LeftLabel{$SomeRuleZ$}
        \UnaryInfC{\assumption{}}
        \DisplayProof

        \bigskip

        rewrites to

        \bigskip

              \AxiomC{\assumption{}}
            \LeftLabel{$SomeRuleW$}
            \UnaryInfC{$\metavar{\Gamma} \entails \metavar{a}$}
              \AxiomC{\assumption{}}
            \RightLabel{$SomeRuleX$}
            \UnaryInfC{$\metavar{a} \cons \metavar{\Gamma} \entails \metavar{c}$}
          \LeftLabel{\satisfy}
          \BinaryInfC{$\metavar{\Gamma} \entails \metavar{c}$}
        \LeftLabel{$SomeRuleZ$}
        \UnaryInfC{\assumption{}}
        \DisplayProof
      }
      \example{\center
                \AxiomC{\assumption{}}
              \LeftLabel{$SomeRuleW$}
              \UnaryInfC{$\metavar{\Gamma} \entails \metavar{b}$}
            \LeftLabel{\orIntroR}
            \UnaryInfC{$\metavar{\Gamma} \entails \metavar{a} \vee \metavar{b}$}
              \AxiomC{\assumption{}}
            \RightLabel{$SomeRuleX$}
            \UnaryInfC{$\metavar{a} \cons \metavar{\Gamma} \entails \metavar{c}$}
              \AxiomC{\assumption{}}
            \RightLabel{$SomeRuleY$}
            \UnaryInfC{$\metavar{b} \cons \metavar{\Gamma} \entails \metavar{c}$}
          \LeftLabel{\orElim}
          \TrinaryInfC{$\metavar{\Gamma} \entails \metavar{c}$}
        \LeftLabel{$SomeRuleZ$}
        \UnaryInfC{\assumption{}}
        \DisplayProof

        \bigskip

        rewrites to

        \bigskip

              \AxiomC{\assumption{}}
            \LeftLabel{$SomeRuleW$}
            \UnaryInfC{$\metavar{\Gamma} \entails \metavar{b}$}
              \AxiomC{\assumption{}}
            \RightLabel{$SomeRuleY$}
            \UnaryInfC{$\metavar{b} \cons \metavar{\Gamma} \entails \metavar{c}$}
          \LeftLabel{\satisfy}
          \BinaryInfC{$\metavar{\Gamma} \entails \metavar{c}$}
        \LeftLabel{$SomeRuleZ$}
        \UnaryInfC{\assumption{}}
        \DisplayProof
      }
    \end{center}

    Like with the implication detours, we can always use the ``plugging in'' method to get rid of these uses of \satisfy\ if we want to.



\section{\acrshort{ipl}}


  At this point, we have all of the \vocabs{rule-of-inference} for the \vocab{positive-fragment} of \acrshort{ipl}, along with rules for manipulating our \vocabs{context} of \vocabs{assumption}. Here they are all together, not including the \vocabs{derived-rule}:

  \begin{center}
    \Here \quad \There \quad \Assumed

    \SubNil \quad \SubCons

    \TopIntro

    \AndIntro \quad \AndElimL \quad \AndElimR

    \OrIntroL \quad \OrIntroR

    \OrElim

    \ImplIntro \quad \ImplElim

  \end{center}

  You're encouraged to play around with these rules and see what you can prove! It's a good way to build intuition for what \acrshort{ipl} is and how it works in general.



\section{Meaning}

  We've been staring at trees for a while now, so let's take a step back and discuss what all this means at a higher level.


  \subsection{Arbitrary constants}

    We keep using symbols like \mono{P} and \mono{R} in our proofs to represent ``arbitrary constants'', as we defined when we originally listed the operators of \acrshort{ipl}. What exactly is an ``arbitrary proposition'' again?

    To be clear, we're mostly using arbitrary constants in our proofs because we haven't defined any actual mathematical objects to prove things about yet. We'll eventually be proving things about objects like numbers and lists, and then we won't have too much use for these arbitrary constants.

    When we prove a proposition that involves arbitrary constants, we're really proving something about the operators of our proof system itself. For example, we can prove that \mbox{$\left(\mono{P} \vee \mono{Q}\right) \subset \left(\mono{Q} \vee \mono{P}\right)$} is a \vocab{tautology}, which means that it is valid to ``swap'' a disjunction between \insist{any} two propositions we might choose. Facts like this will remain valid even when we have other mathematical objects in our system.

    Our use of arbitrary constants is standard in \vocab{propositional-logic}, but soon we'll see that we can make all of this reasoning somewhat more formal using \vocab{predicate-logic}. In one sense, our use of arbitrary constants is just making up for the fact that we don't have a \vocab{universal-quantifier} in our proof system yet.


  \subsection{The BHK interpretation}

    What exactly is the ``intuition'' in \vocabstyle{\acrlong{ipl}}?

    Historically, \acrshort{ipl} dates back to the early 20th century, when there was a lot of active work on the philosophical foundations of logic. Several famous mathematicians and logicians had challenged the conventionally-accepted definitions of logical operators, so the mathematical community began looking for well-defined systems of logic that had clear explanations in terms of ``intuition''.

    The word ``intuition'' is generally used in mathematics to suggest a kind of ``common sense'' about how math works. Historically, there has been a lot of philosophical discussion and debate over what exactly ``intuition'' is made of, with proposed answers coming from all across fields like philosophy, physics, psychology, and neuroscience.

    The very broad idea of ``intuition'' is that people already have an experience of how the world ``works'' before they learn about formal logic, so we can try to use that experience to justify why logic ``works''. This idea has inspired several popular explanations of \acrshort{ipl}.

    The \linkstyle{\href{https://en.wikipedia.org/wiki/Brouwer\%E2\%80\%93Heyting\%E2\%80\%93Kolmogorov\_interpretation}{Brouwer-Heyting-Kolmogorov (BHK) interpretation}} of \acrshort{ipl} provides a very direct ``intuitive'' explanation of the operators we've defined so far, and a couple others in addition. This presentation of \acrshort{ipl} was one of the factors that led to this form of logic being called ``intuitionistic''.

    The BHK interpretation allows, and in some sense requires, that we assume some things are ``true'' outside of formal logic: the interpretation is only really meaningful if we assume that there are things we can ``prove'' \insist{without} using \acrshort{ipl}. Other mathematical and logical theories can fill this space, or we can just lean on intuition and say that some ``obvious'' real-world facts are ``provable'' without formal logic.

    The BHK interpretation doesn't directly say anything about assumptions, only about the operators of \acrshort{ipl}. Building on top of our existing understanding of ``proof'', the BHK interpretation gives a meaning to each operator of \acrshort{ipl}. These meanings are fundamentally informal, but are meant to be as precise as reasonably possible without relying on formal logic.

    Here's a somewhat modernized phrasing, leaning on a bit of programming intuition:

    \begin{itemize}
      \item A proof of $\emptyset \entails P \wedge Q$ is a pair of proofs, containing a proof of $\emptyset \entails P$ and a proof of $\emptyset \entails Q$.
      \item A proof of $\emptyset \entails P \vee Q$ is either a proof of $\emptyset \entails P$ or a proof of $\emptyset \entails Q$, along with a ``tag'' indicating which proof.
      \item A proof of $\emptyset \entails P \subset Q$ is a procedure to convert a proof of $\emptyset \entails P$ into a proof of $\emptyset \entails Q$.
    \end{itemize}

    At this point, it's hopefully not too hard to see how the meanings of the $\wedge$ and $\vee$ operator apply in our proof system. The $\subset$ operator is more subtle: what exactly is a ``procedure'', and how do \implIntro\ and \implElim\ relate to ``procedures''?

    There is some historical debate about what exactly Brouwer and Heyting meant by ``procedure'' (or what Kolmogorov meant by his choice of terminology in Russian), but today it's very common to read this part of the BHK interpretation as a reference to computability theory.

    The precise role of \implIntro\ and \implElim\ in this story will become more clear when we introduce \vocab{lambda-calculus}, but essentially \implIntro\ corresponds to \vocab{function-definition} and \implElim\ corresponds to \vocab{function-application} in the computational reading of our proof system. We use \implIntro\ to create a kind of ``template'' proof that can be ``instantiated'' by using \implElim\ with an ``argument'' proof.



\input{include/footer.tex}
\end{document}
