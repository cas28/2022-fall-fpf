\input{include/header.tex}

% auto-generated glossary terms
\input{notes/0.glt}

\indextitle{Introduction}
\subtitle{What are proofs?}

\makeglossaries


\begin{document}

\maketitle
\tableofcontents

\newpage



\section{Course information}


  \subsection{Introduction}

    Welcome to CS 410/510 Formal Proof Foundations!

    \insist{Please read the \href{https://canvas.pdx.edu/courses/55236/files/6024612?module\_item\_id=2837373}{\linkstyle{syllabus}} on Canvas.}

    These notes will be your main reading material in this course, so always make sure to read them carefully and reach out if you have any questions about them!

    In lecture, we will discuss the notes and work through examples. You will get the most value out of the lectures if you read the notes \insist{before} attending or watching each lecture. See Canvas for the \href{https://canvas.pdx.edu/courses/55236/files/6024613?module\_item\_id=2837374}{\linkstyle{lecture schedule}}.


  \subsection{Style conventions}

    The font styles that I use in these notes are meaningful:

    \begin{itemize}
      \item \Vocab{italicized-text} is used for vocabulary terms, when I'm introducing a new term or reminding you of a term that I expect you've seen before. These are the terms that are listed in the \hyperref[sec:glossary]{``\textbf{Key terms and symbols}''} section at the end of each set of notes. They're also clickable!
      \item \insist{Bold text} is used for general emphasis, when I want to really make sure you don't miss something.
      \item \mono{Monospace text} is used for code, which will often use colored font for syntax highlighting.
      \item \rulename{Small caps} are used for the names of rules of inference in our proof systems.
    \end{itemize}

    Similarly, the boxes that you'll see in these notes are meaningful:

    \longexample{A solid outline indicates a question or example that is worked out completely in the notes.}

    \longdemo{A dashed outline indicates a question or example that will be discussed in lecture (time permitting).}


  \subsection{Terminology}

    Keep in mind that almost no piece of terminology is universal: if you read a variety of sources about logic and proofs, you will encounter a bunch of different terms for the same concepts. This is unfortunate in some ways, but it's unavoidable in a global field that dates back thousands of years.

    I have certain biases in my choice of terminology for this course: we will eventually cover \vocab{classical-logic}, but our ``default'' setting will be \vocab{constructive-logic}, so our terminology will mostly come from the tradition of constructive logic. Don't worry if you don't know what that means! It'll become clearer as we get into the course material.

    If you're ever unsure of whether a term means what you think it means, please don't hesitate to ask! You may have to ``forget'' a few words that you already know for this course in order to learn new definitions of them, but you may also already know some synonyms for some of the terms that we introduce.

    The traditions of logic and proof theory tend to use a lot of \linkstyle{\href{https://en.wikipedia.org/wiki/Greek\_alphabet}{Greek letters}}. It will be helpful to keep a cheat sheet of Greek letters and their English names if you're not already familiar with them.

    \longdemo{We're going to introduce a lot of symbols in this course that will probably be new to you, which can be a bit overwhelming at first. How can you study effectively when you're drowning in unfamiliar notation?}


  \subsection{Source code}

    \longdemo{What is \LaTeX, and how does it relate to this course?}

    The \vocab{sym-latex}\ source code of these notes is available at \href{https://gitlab.cecs.pdx.edu/cas28/2022-fall-fpf}{\linkstyle{GitLab}}.

    Don't worry! \insist{You are not expected to use \LaTeX\ in this course}; I'm just providing the source code for anyone who's curious. If you \insist{want} to practice writing documents in \LaTeX, this course can be a good opportunity for that.

    These notes will sometimes contain code in Agda, a proof language that we'll be working with. Whenever there is usable Agda code in the notes, there will also be a corresponding \mono{.agda} source code file on GitLab containing the raw Agda code in a form that can be directly compiled.



\section{Course goals}


  \subsection{Proving}

    In general, I want this course to help you with reading and writing proofs.

    In the coursework, you'll get practice with two different kinds of proving:

    \begin{itemize}
      \item ``manual'' proving, checking your own work against a set of rules
      \item ``assisted'' proving, using a computer to guide and check your work
    \end{itemize}

    We'll start with proofs about basic logical and arithmetic concepts, and by the end of the course we'll be writing proofs about the behavior of simple programs.


  \subsection{Proof theory}

    Besides working with proofs in practice, we will also spend quite a bit of time introducing the study of \vocab{proof-theory} from the ground up. This will be the main theoretical content of this course.

    In very rough terms, proof theory is the study of how to \insist{write} proofs and how to \insist{check} proofs. More technically, proof theory is a logical study of how to define, analyze, and manipulate certain specific kinds of ``proof structures'' (which we just call \vocabs{proof} in context).

    It probably won't be obvious at first, but we will eventually see how the idea of ``proof structures'' in proof theory is actually very similar to the idea of \vocabs{data-structure} in computer science. More precisely, we'll see how a \vocab{proof} can be defined as a particular kind of \vocab{tree} structure, and \vocab{proof-checking} can be defined as an \vocab{algorithm} that analyzes a proof's tree structure.

    We'll start with the very basics of proof theory, assuming no prior exposure. By the end of the course, we'll have precise definitions for all of the core concepts needed to write and check proofs about simple programs.

    \longdemo{What's the best definition of the word ``proof'' that you can give right now, before working through this course material?}


  \subsection{Proof assistants}

    In our study of assisted proving, we will investigate the concept of a \vocab{proof-assistant} and work with the Agda proof assistant in particular.

    In very rough terms, a proof assistant is an application that helps the user write proofs and is able to check whether a given proof is correct. More technically, proof assistants implement the proof-checking algorithms that come from the study of proof theory, allowing a computer to automatically check and analyze user-entered proofs in various ways.

    We'll eventually see how the idea of a proof assistant is actually very similar to the idea of a \vocab{programming-language} in computer science, and in fact the study of proof assistants is generally thought of as part of the study of programming language theory. Under this interpretation, a proof corresponds to a particular kind of \vocab{program}, and proof checking can be defined as a \vocab{typechecking} algorithm.

    We'll start with the very basics of Agda, assuming no prior exposure to proof assistants. By the end of the course, we'll have built ``from scratch'' everything we need to write proofs about simple programs in Agda.

    \longdemo{How are proof assistants being used in the ``real world'' of computer science and mathematics?}



\section{First definitions}


  Before we get into the specifics of proof theory, let's get a feel for some of the most fundamental vocabulary terms that the whole field relies on. We will give more precise definitions for each of these terms later, but it'll be helpful to have a bit of intuition first.


  \subsection{Propositions}

    A \vocab{proof} tells us about the \vocab{truth} or \vocab{value} of something else. That ``something else'' is called a \vocab{proposition}.

    Proofs and propositions are intimately related, but are not the same! A proposition is like a \insist{question}; a proof is like an \insist{answer}. The precise relationship between proofs and propositions is central to the study of \vocab{proof-theory}.

    Every proof is a proof \insist{of some proposition}. If you want to prove something, you have to know exactly what you're trying to prove first!

    \subsubsection*{Examples}

      Here are some example propositions:

      \begin{itemize}
        \item 1 + 1 = 2
        \item 1 + 1 = 3
        \item January 1, 1900 was a Saturday.
        \item The \linkstyle{\href{https://en.wikipedia.org/wiki/Collatz\_conjecture}{Collatz conjecture}} is true.
      \end{itemize}

      Important things to note:

      \begin{itemize}
        \item A statement can be a proposition even if it's not true.
        \item A statement can be a proposition even if you don't know whether it's true.
        \item A statement can be a proposition even if nobody knows whether it's true.
      \end{itemize}

    \subsubsection*{Common non-examples}

      Are \insist{all} statements \vocab{propositions}?

      Not really: for one thing, propositions must be \insist{unambiguous}. A statement like ``She went over there'' is too \insist{ambiguous} out of context to be ``true'' or ``false'', although it could be part of a larger proposition that provides the missing context.

      We also generally reserve the word \vocab{proposition} for statements that we have some intention of analyzing \insist{logically}. Proof theory doesn't claim to have the only definition of ``meaning''; there are many kinds of statements that are widely considered to be ``meaningful'' but outside the domain of \insist{logic}.

      For example, poetry and other art forms may use language that looks superficially ``propositional'', but they usually work at a level of emotional ``truth'' that isn't well-served by a fully logical analysis of ``truth''. With that in mind, we usually wouldn't consider something like a poem or a novel to be a ``proposition'' in proof theory, because we probably have no intention to consider its logical truth value.

    \subsubsection*{Weird non-examples}

      Are these sentences propositions?

      \begin{enumerate}
        \item This sentence is true.
        \item This sentence is false.
      \end{enumerate}

      They're a little weird to think about: both sentences are self-referential in a way that seems to mess with their truth values. The second one in particular seems like it can't be true or false, or must somehow be both at the same time.

      In proof theory, we usually say these are \insist{not} propositions. We'll dig into how this works later; for now, just know that we have a way to rule out directly self-referential statements like these when we talk about propositions.


  \subsection{Truth}

    What is \vocab{truth} (in proof theory)?

    Truth is a property that some \vocab{propositions} have. If we have a \vocab{proof} of some proposition, then we know that the proposition is true.

    Can something be logically true even if we \insist{don't} have a proof of it?

    This turns out to be a complex question! For now, we'll say \insist{no}: \mbox{``truth'' = ``provability''}, \mbox{``true'' = ``proven''}. This means a proof is the \insist{only} way to show that a proposition is true. Later, we'll explore the consequences of this terminology choice.

    (If you're wondering about the ``incompleteness theorem'', this doesn't contradict it, but it does give us a slightly different way to look at it.)


  \subsection{Proofs}

    What exactly is a \vocab{proof}?

    There are actually lots of different answers!

    We will explore the definition of \vocab{proof} in much more depth throughout this course. For now, we'll just say that a proof of a \vocab{proposition} is some particular form of \insist{evidence} for the proposition.


  \subsection{Proof checking}

    What if a proof has mistakes in it? Well, then it's not a \vocab{proof}, technically: it's only a proof if it's correct.

    How can we tell whether an attemped ``proof'' is correct?

    In general, we have procedures to \vocab{check} a proof. Different kinds of proofs are checked in different ways.

    Proof checking is one of the fundamental concerns of proof theory. When we consider how to write proofs, a high priority is for our proofs to be easily checkable.


  \subsection{Informal language}
    A \vocab{natural-language} is a language that humans use to communicate with each other. Natural languages include English, Arabic, Swedish, Korean, and many many more.

    In the context of proof theory, we say natural languages are \vocab{informal-languages}. When we write in English, we are writing \insist{informally}.

    \longdemo{How do \vocabs{informal-proof} work?}


  \subsection{Formal language}
    A \vocab{formal-language} is a kind of language designed for logical analysis and manipulation. You can study a more precise definition of the term \vocab{formal-language} in a course like CS 311 or CS 581.

    All \vocabs{programming-language} are formal languages. When we write programs in a programming language, we are writing \insist{formally}, which is meaningfully different than writing \insist{informally}.

    Natural languages like English allow \insist{vagueness} and \insist{ambiguity}. If two people read a piece of English writing, they may understand it in different ways.

    This can be good for things like poetry, but is bad for proofs. If two people read a proof, they should ideally understand it the \insist{same} way!

    Formal language minimizes vagueness and ambiguity. Writing a proof in formal language ensures that we can \vocab{check} the proof easily.

    What does a \vocab{formal-proof} look like? We'll see very soon!


\input{include/footer.tex}

\end{document}
